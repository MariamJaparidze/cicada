2.0
-----------------------
BrightMatter� Plan automatically generates high-fidelity, dynamic, whole-brain tractography. You can easily review tracts and explore multiple surgical approaches to create powerful pre-operative plans�for every case. Use tractography, without disrupting surgical workflow, to confirm the data is concordant with your intended approach and determine whether detailed surgical planning is required. Then, effortlessly export your surgical plans for navigation in the operating room.


2.1
-----------------------
Tractography research

2.2
-----------------------
Tractography Videos



2.2.1
-----------------------
Legacy Health

2.2.2
-----------------------
arcuate segmentation

2.2.3
-----------------------
access tractography in under 30 seconds



3.0
-----------------------
The increased magnification and larger field of view of Modus V� enable you to clearly see a broader range of anatomy, while autofocus and a greater standoff distance reduce workflow disruptions.

3.1
-----------------------
modus v� Visualization



5.0
-----------------------
The increased magnification and larger field of view of Modus V� enable you to clearly see a broader range of anatomy, while autofocus and a greater standoff distance reduce workflow disruptions.

5.1
-----------------------
modus v� Automation



6.0
-----------------------
Automate your surgical workflow

6.1
-----------------------
Robotically Assisted Manual Motions

6.2
-----------------------
Hands-Free Robotic Control

6.3
-----------------------
Fast Room Setup

6.4
-----------------------
Ergonomics


9.0
-----------------------
Benefits for Humanity: From Space to Surgery

A NASA documentary published February 22, 2018, explores the space robotics origins of Synaptive Medical�s robotic digital microscopes, Modus V� and BrightMatter� Drive. The video examines how technology originally used to create Canadarm2 on the International Space Station has been adapted to healthcare and is now benefiting patients with brain cancer and spine diseases.

9.1
-----------------------
Benefits for Humanity: From Space to Surgery