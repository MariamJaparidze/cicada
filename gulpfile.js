var gulp        = require('gulp');
var sass        = require('gulp-sass');
var connect     = require('gulp-connect');
var concat      = require('gulp-concat');
var minifyCss   = require('gulp-clean-css');
var version     = require('gulp-version-number');
var versionConfig = {
  'value': '%MDS%',
  'append': {
    'key': 'v',
    'to': ['css'],
  },
};

gulp.task('run',['build', 'watch'], function () {
    connect.server({
        root: '',
        port: 3002,
        livereload: true,

        middleware: function(connect, opt) {
          return [];
        }
    })
});

// SASS AUTOMATE
gulp.task('sass', function() {
    return gulp.src([
        'assets/sass/_base.scss',
        'assets/sass/device_ratio_mixin.scss',
        'assets/sass/**/*.scss'
        ])
        .pipe(concat('main.min.css'))
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest('assets'))
});

// WATCH
gulp.task('watch', function(){
    gulp.watch('assets/sass/**/*.scss', ['sass']);
});

//BUILD
gulp.task('build', ['sass'], function () {
    console.log('Building files');
});